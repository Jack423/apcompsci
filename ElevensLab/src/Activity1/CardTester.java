package Activity1;

import java.util.Scanner;

/**
 * This is a class that tests the Card class.
 */
public class CardTester {

    public static void main(String[] args) {
        Card[] myDeck = new Card[3];
        
        Scanner kb = new Scanner(System.in);
        int suitPicker;
        int rankPicker;
        String suit = "";
        String rank = "";
        int value = 0;
        
        for(int i = 0; i < myDeck.length; i++) {
            suitPicker = (int) ((Math.random()+1)*4);
            if(suitPicker == 1) {
                suit = "hearts";
            } else if(suitPicker == 2) {
                suit = "clubs";
            } else if(suitPicker == 3) {
                suit = "spades";
            } else if(suitPicker == 4) {
                suit = "diamonds";
            }
            
            rankPicker = (int) Math.random() + 1 * 13;
            if(rankPicker == 1) {
                rank = "ace";
                value = 1;
            } else if(rankPicker == 2) {
                rank = "two";
                value = 2;
            } else if(rankPicker == 3) {
                rank = "three";
                value = 3;
            } else if(rankPicker == 4) {
                rank = "four";
                value = 4;
            } else if(rankPicker == 5) {
                rank = "five";
                value = 5;
            } else if(rankPicker == 6) {
                rank = "six";
                value = 6;
            } else if(rankPicker == 7) {
                rank = "seven";
                value = 7;
            } else if(rankPicker == 8) {
                rank = "eight";
                value = 8;
            } else if(rankPicker == 9) {
                rank = "nine";
                value = 9;
            } else if(rankPicker == 10) {
                rank = "ten";
                value = 10;
            } else if(rankPicker == 11) {
                rank = "jack";
                value = 11;
            } else if(rankPicker == 12) {
                rank = "queen";
                value = 12;
            } else if(rankPicker == 13) {
                rank = "king";
                value = 13;
            }
            
            myDeck[i] = new Card(rank, suit, value);
        }
        
        for(int i = 0; i < myDeck.length; i++) {
            System.out.println(myDeck[i].suit());
            System.out.println(myDeck[i].rank());
            System.out.println(myDeck[i].pointValue());
            if(i < 3) {
                System.out.println(myDeck[i].matches(myDeck[i+1]));
            }
        }
    }
}
