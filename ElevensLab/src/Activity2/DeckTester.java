package activity2;

import static java.lang.System.out;

/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {

	/**
	 * The main method in this class checks the Deck operations for consistency.
	 *	@param args is not used.
	 */

	static String[] deck1Ranks = {"", " ", "  ", "   "};
	static String[] deck1Suits = {"suede", "striped", "tweed"};
	static int[] deck1Vals = {0, 1, 2, 3};
	static String[] deck2Ranks = {"a","aa","aaa","aaaa"};
	static String[] deck2Suits = {"space", "diving", "swim"};
	static int[] deck2Vals = {1, 2, 3, 4};	
	static String[] deck3Ranks = {"b","bb","bbb","bbbb"};
	static String[] deck3Suits = {"je", "pur", "law"};
	static int[] deck3Vals = {2, 3, 4, 5};
	
	public static void main(String[] args) {
		Deck deck1 = new Deck(deck1Ranks, deck1Suits, deck1Vals);
		Deck deck2 = new Deck(deck2Ranks, deck2Suits, deck2Vals);
		Deck deck3 = new Deck(deck3Ranks, deck3Suits, deck3Vals);
		
		out.println(deck1.isEmpty());
		out.println(deck2.isEmpty());
		out.println(deck3.isEmpty());
		
		out.println(deck1.size());
		out.println(deck2.size());
		out.println(deck3.size());
		
		out.println(deck1.deal());
		out.println(deck2.deal());
		out.println(deck3.deal());
	}
}
