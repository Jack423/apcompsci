package Activity3;

import Activity1.Card;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class provides a convenient way to test shuffling methods.
 */
public class Shuffler extends Card {

    /**
     * The number of consecutive shuffle steps to be performed in each call to
     * each sorting procedure.
     */
    private static final int SHUFFLE_COUNT = 1;

    /**
     * The number of values to shuffle.
     */
    private static final int VALUE_COUNT = 4;

    /**
     * Tests shuffling methods.
     *
     * @param args is not used.
     */
    public static void main(String[] args) {
        System.out.println("Results of " + SHUFFLE_COUNT
                + " consecutive perfect shuffles:");
        int[] values1 = new int[VALUE_COUNT];
        for (int i = 0; i < values1.length; i++) {
            values1[i] = i;
        }
        for (int j = 1; j <= SHUFFLE_COUNT; j++) {
            perfectShuffle(values1);
            System.out.print("  " + j + ":");
            for (int k = 0; k < values1.length; k++) {
                System.out.print(" " + values1[k]);
            }
            System.out.println();
        }
        System.out.println();

        System.out.println("Results of " + SHUFFLE_COUNT
                + " consecutive efficient selection shuffles:");
        int[] values2 = new int[VALUE_COUNT];
        for (int i = 0; i < values2.length; i++) {
            values2[i] = i;
        }
        for (int j = 1; j <= SHUFFLE_COUNT; j++) {
            selectionShuffle(values2);
            System.out.print("  " + j + ":");
            for (int k = 0; k < values2.length; k++) {
                System.out.print(" " + values2[k]);
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Apply a "perfect shuffle" to the argument. The perfect shuffle algorithm
     * splits the deck in half, then interleaves the cards in one half with the
     * cards in the other.
     *
     * @param values is an array of integers simulating cards to be shuffled.
     */
    public static void perfectShuffle(int[] values) {
        int[] shuffled = new int[values.length];
        int k = 0;
        for (int j = 0; j < (values.length / 2); j++) {
            shuffled[k] = values[j];
            k += 2;
        }

        k = 1;

        for (int j = (values.length / 2); j < values.length; j++) {
            shuffled[k] = values[j];
            k += 2;
        }
    }

    public static String flip() {
        int flipper = (int) (Math.random() * 2);
        if (flipper == 0) {
            return "Tails";
        } else {
            return "Heads";
        }
    }

    public static boolean arePermutations(int[] x, int[] y) {
        int arrayCounter = 0;

        Arrays.sort(x);
        Arrays.sort(y);

        for (int spot = 0; spot < x.length; spot++) {
            if (x[spot] == y[spot]) {
                arrayCounter++;
            } else {
                return false;
            }
        }
        if (arrayCounter == x.length) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Apply an "efficient selection shuffle" to the argument. The selection
     * shuffle algorithm conceptually maintains two sequences of cards: the
     * selected cards (initially empty) and the not-yet-selected cards
     * (initially the entire deck). It repeatedly does the following until all
     * cards have been selected: randomly remove a card from those not yet
     * selected and add it to the selected cards. An efficient version of this
     * algorithm makes use of arrays to avoid searching for an as-yet-unselected
     * card.
     *
     * @param values is an array of integers simulating cards to be shuffled.
     */
    public static void selectionShuffle(int[] values) {
        int r, place;

        for (int k = values.length - 1; k > 0; k--) {
            r = (int) (Math.random() * k);

            place = values[k];
            values[k] = values[r];
            values[r] = place;
        }
    }

    public Shuffler(String cardRank, String cardSuit, int cardPointValue) {
        super(cardRank, cardSuit, cardPointValue);
    }
}
