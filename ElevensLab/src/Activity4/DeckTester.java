package Activity4;

/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {

    /**
     * The main method in this class checks the Deck operations for consistency.
     *
     * @param args is not used.
     */
    public static void main(String[] args) {
        String[] ranks = {"jack", "queen", "king"};
        String[] suits = {"blue", "red"};
        int[] pointValues = {11, 12, 13};
        Deck d = new Deck(ranks, suits, pointValues);

        System.out.println("**** Original Deck Methods ****");
        System.out.println("  toString:\n" + d.toString());
        System.out.println("  isEmpty: " + d.isEmpty());
        System.out.println("  size: " + d.size());
        System.out.println();
        System.out.println();

        System.out.println("**** Deal a Card ****");
        System.out.println("  deal: " + d.deal());
        System.out.println();
        System.out.println();

        System.out.println("**** Deck Methods After 1 Card Dealt ****");
        System.out.println("  toString:\n" + d.toString());
        System.out.println("  isEmpty: " + d.isEmpty());
        System.out.println("  size: " + d.size());
        System.out.println();
        System.out.println();

        System.out.println("**** Deal Remaining 5 Cards ****");
        for (int i = 0; i < 5; i++) {
            System.out.println("  deal: " + d.deal());
        }
        System.out.println();
        System.out.println();

        System.out.println("**** Deck Methods After All Cards Dealt ****");
        System.out.println("  toString:\n" + d.toString());
        System.out.println("  isEmpty: " + d.isEmpty());
        System.out.println("  size: " + d.size());
        System.out.println();
        System.out.println();

        System.out.println("**** Deal a Card From Empty Deck ****");
        System.out.println("  deal: " + d.deal());
        System.out.println();
        System.out.println();
        
        int suitPicker;
        int rankPicker;
        String suit = "";
        String rank = "";
        int value = 0;
        
        String[] fullRanks = new String[52];
        String[] fullSuits = new String[52];
        int[] fullPointValues = new int[52];

        for(int i = 0; i < 52; i++) {
            suitPicker = (int) ((Math.random()+1)*4);
            if(suitPicker == 1) {
                suit = "hearts";
            } else if(suitPicker == 2) {
                suit = "clubs";
            } else if(suitPicker == 3) {
                suit = "spades";
            } else if(suitPicker == 4) {
                suit = "diamonds";
            }
            
            fullSuits[i] = suit;
            
            rankPicker = (int) Math.random() + 1 * 13;
            if(rankPicker == 1) {
                rank = "ace";
                value = 1;
            } else if(rankPicker == 2) {
                rank = "two";
                value = 2;
            } else if(rankPicker == 3) {
                rank = "three";
                value = 3;
            } else if(rankPicker == 4) {
                rank = "four";
                value = 4;
            } else if(rankPicker == 5) {
                rank = "five";
                value = 5;
            } else if(rankPicker == 6) {
                rank = "six";
                value = 6;
            } else if(rankPicker == 7) {
                rank = "seven";
                value = 7;
            } else if(rankPicker == 8) {
                rank = "eight";
                value = 8;
            } else if(rankPicker == 9) {
                rank = "nine";
                value = 9;
            } else if(rankPicker == 10) {
                rank = "ten";
                value = 10;
            } else if(rankPicker == 11) {
                rank = "jack";
                value = 11;
            } else if(rankPicker == 12) {
                rank = "queen";
                value = 12;
            } else if(rankPicker == 13) {
                rank = "king";
                value = 13;
            }
            
            fullRanks[i] = rank;
            fullPointValues[i] = value;
        }
        
        Deck full = new Deck(fullRanks, fullSuits, fullPointValues);
        
        full.shuffle();
    }
}
