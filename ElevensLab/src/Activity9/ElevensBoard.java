package Activity9;

import java.util.List;
import java.util.ArrayList;

/**
 * The ElevensBoard class represents the board in a game of Elevens.
 */
public class ElevensBoard extends Board {

    /**
     * The size (number of cards) on the board.
     */
    private static final int BOARD_SIZE = 9;

    /**
     * The ranks of the cards for this game to be sent to the deck.
     */
    private static final String[] RANKS
            = {"ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"};

    /**
     * The suits of the cards for this game to be sent to the deck.
     */
    private static final String[] SUITS
            = {"spades", "hearts", "diamonds", "clubs"};

    /**
     * The values of the cards for this game to be sent to the deck.
     */
    private static final int[] POINT_VALUES
            = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 0, 0};

    /**
     * Flag used to control debugging print statements.
     */
    private static final boolean I_AM_DEBUGGING = false;

    /**
     * Creates a new <code>ElevensBoard</code> instance.
     */
    public ElevensBoard() {
        super(BOARD_SIZE, RANKS, SUITS, POINT_VALUES);
    }

    /**
     * Determines if the selected cards form a valid group for removal. In
     * Elevens, the legal groups are (1) a pair of non-face cards whose values
     * add to 11, and (2) a group of three cards consisting of a jack, a queen,
     * and a king in some order.
     *
     * @param selectedCards the list of the indices of the selected cards.
     * @return true if the selected cards form a valid group for removal; false
     * otherwise.
     */
    @Override
    public boolean isLegal(List<Integer> selectedCards) {
        int sum = 0;

        for (int currentCard = 0; currentCard < selectedCards.size(); currentCard++) {
            sum += selectedCards.get(currentCard);
        }

        if (sum == 11) {
            return true;
        } else if (sum == 36) {
            return true;
        }

        return false;
    }

    /**
     * Determine if there are any legal plays left on the board. In Elevens,
     * there is a legal play if the board contains (1) a pair of non-face cards
     * whose values add to 11, or (2) a group of three cards consisting of a
     * jack, a queen, and a king in some order.
     *
     * @return true if there is a legal play left on the board; false otherwise.
     */
    @Override
    public boolean anotherPlayIsPossible() {
        for (int i = 0; i < cards.length; i++) {
            for (int j = 0; j < cards.length; j++) {
                for (int k = 0; k < cards.length; k++) {

                    List<Integer> indexes = new ArrayList<Integer>();

                    indexes.add(i);
                    indexes.add(j);
                    indexes.add(k);

                    if (containsJQK(indexes)) {
                        return true;
                    }
                }
            }
        }

        //Check for cards adding up to 11
        //Check for JQK on board
        for (int i = 0; i < cards.length; i++) {
            for (int j = 0; j < cards.length; j++) {

                List<Integer> indexes = new ArrayList<Integer>();

                indexes.add(i);
                indexes.add(j);

                if (containsPairSum11(indexes)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check for an 11-pair in the selected cards.
     *
     * @param selectedCards selects a subset of this board. It is list of
     * indexes into this board that are searched to find an 11-pair.
     * @return true if the board entries in selectedCards contain an 11-pair;
     * false otherwise.
     */
    private boolean containsPairSum11(List<Integer> selectedCards) {
        //Get the two cards
        Card card1 = super.cardAt(selectedCards.get(0));
        Card card2 = super.cardAt(selectedCards.get(1));

        //Check to make sure the two cards add to eleven
        if(card1.pointValue() + card2.pointValue() != 11) {
            return false;
        }

        return true;
    }

    /**
     * Check for a JQK in the selected cards.
     *
     * @param selectedCards selects a subset of this board. It is list of
     * indexes into this board that are searched to find a JQK group.
     * @return true if the board entries in selectedCards include a jack, a
     * queen, and a king; false otherwise.
     */
    private boolean containsJQK(List<Integer> selectedCards) {
        //Instantiate the three cards that the user selected
        Card one = super.cardAt(selectedCards.get(0));
        Card two = super.cardAt(selectedCards.get(1));
        Card three = super.cardAt(selectedCards.get(2));

        //Checks to see if the cards selected are jack queen or a king
        if(one.rank().equals("jack") ^ two.rank().equals("jack") ^ three.rank().equals("jack")) {
            if(one.rank().equals("queen") ^ two.rank().equals("queen") ^ three.rank().equals("queen")) {
                if(one.rank().equals("king") ^ two.rank().equals("king") ^ three.rank().equals("king")) {
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}
