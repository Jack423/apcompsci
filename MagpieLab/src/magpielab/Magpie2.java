package MagpieLab;

public class Magpie2 {

    /**
     * Get a default greeting
     *
     * @return a greeting
     */
    public String getGreeting() {
        return "Hello, let's talk.";
    }

    /**
     * Gives a response to a user statement
     *
     * @param statement the user statement
     * @return a response based on the rules given
     */
    public String getResponse(String statement) {
        String response = "";
        statement.trim();
        if (statement.length() < 1) {
            response = "Say something, please.";
        } else if (statement.indexOf("no") >= 0) {
            response = "Why so negative?";
        } else if (statement.indexOf("mother") >= 0
                || statement.indexOf("father") >= 0
                || statement.indexOf("sister") >= 0
                || statement.indexOf("brother") >= 0) {
            response = "Tell me more about your family.";
        } else if (statement.indexOf("dog") >= 0
                || statement.indexOf("cat") >= 0) {
            response = "Tell me more about your pets.";
        } else if (statement.indexOf("Stremlow") >= 0) {
            response = "All hail the almighty and glorious Pete!";
        } else if (statement.indexOf("food") >= 0) {
            response = "Food is the best. My favorite is pizza.";
        } else if (statement.indexOf("school") >= 0) {
            response = "I remember when I went to school...";
        } else if (statement.indexOf("war") >= 0) {
            response = "All I'm saying is give peace a chance.";
        } else if (statement.indexOf("poop") >= 0) {
            response = "I think I just pooped my poop.";
        } else {
            response = getRandomResponse();
        }
        return response;
    }

    /**
     * Pick a default response to use if nothing else fits.
     *
     * @return a non-committal string
     */
    private String getRandomResponse() {
        final int NUMBER_OF_RESPONSES = 6;
        double r = Math.random();
        int whichResponse = (int) (r * NUMBER_OF_RESPONSES);
        String response = "";

        if (whichResponse == 0) {
            response = "Interesting, tell me more.";
        } else if (whichResponse == 1) {
            response = "Hmmm.";
        } else if (whichResponse == 2) {
            response = "Do you really think so?";
        } else if (whichResponse == 3) {
            response = "You don't say.";
        } else if (whichResponse == 4) {
            response = "Oh, really?";
        } else if (whichResponse == 5) {
            response = "That makes sense.";
        }

        return response;
    }
}