/*
 * Copyright (c) 2014, Jack Butler
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package unit00javaintro;

/**
 *
 * @author Jack Butler
 */
public class AsciiArt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("     ____.              __     .___                             .__   ");
        System.out.println("    |    |____    ____ |  | __ |   | ______   ____  ____   ____ |  |  ");
        System.out.println("    |    \\__  \\ _/ ___\\|  |/ / |   |/  ___/ _/ ___\\/  _ \\ /  _ \\|  | ");
        System.out.println("/\\__|    |/ __ \\\\  \\___|    <  |   |\\___ \\  \\  \\__(  <_> |  <_> )  |__");
        System.out.println("\\________(____  /\\___  >__|_ \\ |___/____  >  \\___  >____/ \\____/|____/");
        System.out.println("              \\/     \\/     \\/          \\/       \\/                   ");
        System.out.println(".\n" +
"───────▄██████████████████▄───────\n" +
"────▄███████████████████████▄─────\n" +
"───███████████████████████████────\n" +
"──█████████████████████████████───\n" +
"─████████████▀─────────▀████████──\n" +
"██████████▀───────────────▀██████─\n" +
"███████▀────────────────────█████▌\n" +
"██████───▄▀▀▀▀▄──────▄▀▀▀▀▄──█████\n" +
"█████▀──────────────────▄▄▄───████\n" +
"████────▄█████▄───────▄█▀▀▀█▄──██▀\n" +
"████──▄█▀────▀██─────█▀────────█▀─\n" +
"─▀██───────────▀────────▄███▄──██─\n" +
"──██───▄▄██▀█▄──▀▄▄▄▀─▄██▄▀────███\n" +
"▄███────▀▀▀▀▀──────────────▄▄──██▐\n" +
"█▄▀█──▀▀▀▄▄▄▀▀───────▀▀▄▄▄▀────█▌▐\n" +
"█▐─█────────────▄───▄──────────█▌▐\n" +
"█▐─▀───────▐──▄▀─────▀▄──▌─────██▐\n" +
"█─▀────────▌──▀▄─────▄▀──▐─────██▀\n" +
"▀█─█──────▐─────▀▀▄▀▀─────▌────█──\n" +
"─▀█▀───────▄────────────▄──────█──\n" +
"───█─────▄▀──▄█████████▄─▀▄───▄█──\n" +
"───█────█──▄██▀░░░░░░░▀██▄─█──█───\n" +
"───█▄───▀▄──▀██▄█████▄██▀─▄▀─▄█───\n" +
"────█▄────▀───▀▀▀▀──▀▀▀──▀──▄█────\n" +
"─────█▄────────▄▀▀▀▀▀▄─────▄█─────\n" +
"──────███▄──────────────▄▄██──────\n" +
"─────▄█─▀█████▄▄────▄▄████▀█▄─────\n" +
"────▄█───────▀▀██████▀▀─────█▄────\n" +
"───▄█─────▄▀───────────▀▄────█▄───\n" +
"──▄█─────▀───────────────▀────█▄──\n" +
"──────────────────────────────────\n" +
"▐▌▐█▄█▌▐▀▀█▐▀▀▌─█▀─█▀─▐▌▐▀█▐▀█─█─█\n" +
"▐▌▐─▀─▌▐▀▀▀▐──▌─▀█─▀█─▐▌▐▀▄▐▀▄─█─█\n" +
"▐▌▐───▌▐───▐▄▄▌─▄█─▄█─▐▌▐▄█▐─█─█▄█");
        System.out.println("░░░░░░░░░░░░░░░░▄▄▄░░░░░░░░░░░░░░░░░\n" +
"░░░░░░░░░░▄▀▀▀▀▀▀▀▀▀▀▀▀▀▄▄▄░░░░░░░░░\n" +
"░░░░░░▄▄▀▀░░░░░░░░░░░░░░░░░▀▄▄░░░░░░\n" +
"░░░░▄█░░░░░░░░░░░░░░░░░░░░░░░░▀▄░░░░\n" +
"░░░▄▀░░░░░░░░░░░░░░░░░░░░░░░░░░▀▄░░░\n" +
"░░█▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░▀█░░\n" +
"░▄█░░░░░░░░░▄▄▄▄▄░░▀▀▀▀░░░░▄░░░░░▀▄░\n" +
"░█░░░░░░░░▄▄▄▄▄▄░██▀▀▀▀██▄▄▄▄░░░░░▀▄\n" +
"█▀░░░░░░▀▀██▀▀▀▀██▀░░░░▀██▀▀▀▀█▀░░░█\n" +
"█░░░░░░░░████░░░░█░░░█░░██░░░██░░░░█\n" +
"█░░░░░░░▀▀██▄▄▄▄█▀░░░█░░▀██▄▄█▀░░░▄▀\n" +
"█░░░░░░░░░▀████▀▀▄▄▄▄██▄▄████▀░░░░█░\n" +
"█░░▄▀░░░░░░░▄▄▄▄██████▀▀██▄▄░░░░░░█░\n" +
"▀▄▀░░▄▄▄█▀▀▀▀████▀█▀███▀▀▀█████▄▄▄█░\n" +
"░▀▄░░▀█░░░░░░░░░▀▀▀████▀▀▀░░░░▄▄▀█░░\n" +
"░░░▀▄░░▀░░░░░░░░░░▀░▀█░░░░░░░░░░█░░░\n" +
"░░░░░▀▄░░░░░░░░░░░░░░▀░░░░░░░░▄▀░░░░\n" +
"░░░░░░░▀▀▄▄░░░░░░░░░░░░░░░░▄▄▀░░░░░░\n" +
"░░░░░░░░░░░▀▀█▄▄▄▄▄▄▄▄▄▄▄█▀▀░░░░░░░░");
    }
    
}
