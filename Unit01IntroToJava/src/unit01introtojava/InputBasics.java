/**
 * Author: Jack Butler
 * 
 * Lab Description: InputBasics
 * 
 * Completion Comments:
**/

package unit00javaintro;
import java.util.Scanner;

public class InputBasics {

    public static void main(String[] args) {
        //Declared variables here
        Scanner keyboard = new Scanner(System.in);
        int intOne, intTwo;
        String s1,s2;
        double dOne, dTwo;
        int intSum = 0;
       
        //Obtain and store use input
        System.out.print("Please enter a number:\n");
        intOne = keyboard.nextInt();
        System.out.println("Please choose a new number: ");
        intTwo = keyboard.nextInt();
        
        //Process the input and show the equation
        intSum = intOne + intTwo;
        System.out.println(intOne + " + " + intTwo + " = " + intSum);
    }
    
}
