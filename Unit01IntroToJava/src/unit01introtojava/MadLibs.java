/*
 * Author: Jack Butler
 * 
 * Lab Description: Mad Libs
 * 
 * Completin Coments:
 */

package unit00javaintro;
import java.util.Scanner;

public class MadLibs {

    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        String relative1, adjective1, adjective2, adjective3, nopir, adjective4, adjective5, veied, bodypart, veiing, nounplural, noun, adverb, verb1, verb2, relative2, personinroom;
        
        System.out.println("RELATIVE: ");
        relative1 = keyboard.nextLine();
        System.out.println("ADJECTIVE: ");
        adjective1 = keyboard.nextLine();
        System.out.println("ADJECTIVE: ");
        adjective2 = keyboard.nextLine();
        System.out.println("ADJECTIVE: ");
        adjective3 = keyboard.nextLine();
        System.out.println("NAME OF PERSON IN THE ROOM: ");
        nopir = keyboard.nextLine();
        System.out.println("ADJECTIVE: ");
        adjective4 = keyboard.nextLine();
        System.out.println("ADJECTIVE");
        adjective5 = keyboard.nextLine();
        System.out.println("VERB ENDING IN ED: ");
        veied = keyboard.nextLine();
        System.out.println("BODY PART: ");
        bodypart = keyboard.nextLine();
        System.out.println("VERB ENDING IN ING: ");
        veiing = keyboard.nextLine();
        System.out.println("NOUN (PLURAL): ");
        nounplural = keyboard.nextLine();
        System.out.println("NOUN: ");
        adverb = keyboard.nextLine();
        System.out.println("VERB: ");
        verb1 = keyboard.nextLine();
        System.out.println("VERB: ");
        verb2 = keyboard.nextLine();
        System.out.println("RElATIVE: ");
        relative2 = keyboard.nextLine();
        System.out.println("PERSON IN THE ROOM: ");
        personinroom = keyboard.nextLine();
        
        System.out.println("");
        System.out.println("Dear " + relative1);
        System.out.println("I am having a(n) " + adjective1 + " time at camp. The counselour is " + adjective2 + " and the food is " + adjective3 + ". I met " + nopir + " and we became " + adjective4 + " friends. Unfortunately, things are " + adjective5 + " and I " + veied + " my " + bodypart + " so we couldn`t go " + veiing + " like everybody else. I need more " + nounplural + " and a " + adverb + " sharpener, so please " + verb1 + " " + verb2 + " more when you " + verb2 + " back.");
    }
    
}
