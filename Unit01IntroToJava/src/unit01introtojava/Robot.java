/*
 * Author: Jack Butler
 * 
 * Lab Desciption: Robot Lab
 *
 * Comments: RUN DIRECTLY AND NOT WITH A RUNNER!
 */

package unit00javaintro;
 
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Robot
{	
    public static void main( String[] args )
    {
    	Image image = null;
        try {
            URL url = new URL("http://blog.spoongraphics.co.uk/wp-content/uploads/2011/robot/robot-character.jpg");
            image = ImageIO.read(url);
        } catch (IOException e) {
        	e.printStackTrace();
        }
 
        JFrame frame = new JFrame();
        frame.setSize(800, 800);
        JLabel label = new JLabel(new ImageIcon(image));
        frame.add(label);
        frame.setVisible(true);
    }
}