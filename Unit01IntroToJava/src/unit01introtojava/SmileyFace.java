/*
Author: Jack Butler
Project Descripton: SmileyFace Lab
Comments:
*/

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Canvas;

public class SmileyFace extends Canvas
{
   public SmileyFace()    //constructor - sets up the class
   {
      setSize(800,600);
      setBackground(Color.WHITE);
      setVisible(true);
   }

   public void paint( Graphics window )
   {
      smileyFace(window);
   }

   public void smileyFace( Graphics window )
   {
      window.setColor(Color.GREEN);
      window.drawString("SMILEY FACE LAB ", 35, 35);

      window.setColor(Color.GREEN);
      window.fillOval(100, 50, 200, 200);
      
      window.setColor(Color.BLACK);
      window.fillOval(155, 100, 10, 20);
      
      window.setColor(Color.BLACK);
      window.fillOval(230, 100, 10, 20);
      
      window.setColor(Color.BLACK);
      window.drawArc(150, 160, 100, 50, 180, 180);

   }
}