/*
 * Author: Jack Butler
 * 
 * Lab Description: StarsAndStripes
 * 
 * Completion Comments:
 */

package unit00javaintro;

public class StarRunner {

    public static void main(String[] args) {
        //Set test to be equal to StarsAndStripes
        StarsAndStripes test = new StarsAndStripes();
        
        //Call the meathods and print what they are storing
        test.printASmallBox();
        test.printTwoBlankLines();
        test.printABigBox();
    }
    
}
