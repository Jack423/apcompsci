/*
 * Author: Jack Butler
 * Lab Description: Variables Lab
 * Comments: 
 */

package unit00javaintro;

public class Variables {
            
    public static void main(String[] args) {
        
        int intOne = 90877;
        byte byteOne = 127;
        short shortOne = -32123;
        long longOne = 999999999;
        float floatOne = 38.5678f;
        double doubleOne = 923.234;
        char charOne = 'A';
        boolean booleanOne = true;
        String stringOne;
        stringOne = "hello World";
        
        
        
        System.out.println("/////////////////////////////////");
        System.out.println("*Jack Butler            09/12/14*");
        System.out.println("*                               *");
        System.out.println("*        integer types          *");
        System.out.println("*                               *");
        System.out.println("*8 bit - byteOne = "+byteOne+"\t\t*");
        System.out.println("*16 bit - shortOne = " + shortOne + "     *");
        System.out.println("*32 bit - intOne = " + intOne + "        *");
        System.out.println("*64 bit - longOne = " + longOne + "   *");
        System.out.println("*                               *");
        System.out.println("*         real types            *");
        System.out.println("*                               *");
        System.out.println("*32 bit - floatOne = " + floatOne + "    *");
        System.out.println("*64 bit - doubleOne = " + doubleOne + "   *");
        System.out.println("*                               *");
        System.out.println("*      other integer types      *");
        System.out.println("*                               *");
        System.out.println("*16 bit - charOne = " + charOne + "           *");
        System.out.println("*                               *");
        System.out.println("*         other types           *");
        System.out.println("*                               *");
        System.out.println("*booleanOne = " + booleanOne + "              *");
        System.out.println("*stringOne = " + stringOne + "        *");
        System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");        
        
    }
    
}
