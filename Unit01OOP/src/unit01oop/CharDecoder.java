package unit01javaoop;

/**
 *
 * @author Jack Butler
 */

public class CharDecoder {
//variables
    char myCh;

//constructor
public CharDecoder(){
    
}

//methods
//decode a character
public char decode(char in){
    //check for lowercase 
    if(in <= 122 && in >= 97 )    // and -> &&  or -> ||
        return (char)(in - 32);
    //check for uppercase
    else if(in <= 90 && in >= 65)
        return (char)(in+32);
    //check for digits 0-9
    else if(in <= 57 && in >= 48)
        return(char)(in+17);
    else
        return '#';
    }
    
}