/**
 * Author: Jack Butler
 * 
 * Project Description: Use stuff
 * 
 * Comments: Use ComputerRunner to run this class then you can
 *           get the program to work
 */

package unit01javaoop;

import java.util.Scanner;

public class Computer {
    //Declare Attributes
    String ramType;
    int ramCapacity;
    int maxRam;
    String storageType;
    int storageCapacity;
    int screenSize;
    String cpuType;
    String caseType;
    boolean isCompOn;
    boolean isOSInstalled;
    double osCapacity;
    String os;
    
    
    public Computer() {
        //Decalres Constructors
        ramType = "DDR4";
        ramCapacity = 8;
        maxRam = 64;
        screenSize = 24;
        cpuType = "AMD";
        caseType = "Full ATX";
        isCompOn = false;
        isOSInstalled = false;
        os = "Windows";
    }
    
    public void computerOn() {
        //Turns the computer on
        isCompOn = true;
    }
    
    public void addMoreRam() {
        //Uses the scanner util for input
        Scanner keyboard = new Scanner(System.in);
        
        //Asks user to input an amout of RAM they want in their computer
        System.out.println("Choose the amount of ram you want to install. You currently have " + ramCapacity + "gb");
        ramCapacity = keyboard.nextInt();
        
        //if the ram ammount is greater than the max RAM, then returns error
        if(ramCapacity > maxRam) {
            System.out.println("The max ram is " + maxRam + ". You chose " + ramCapacity + ". Please try again: ");
            ramCapacity = keyboard.nextInt();
        }
    }
    
    public void chooseCaseType() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Asks for input for a case type
        System.out.println("Choose a case type (Full ATX, ATX, Mini ATX)");
        caseType = keyboard.nextLine();
    }
    
    public void cpuType() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Asks for user input for a CPU type
        System.out.println("Choose what CPU you want in your computer (Intel or AMD): ");
        cpuType = keyboard.nextLine();
    }
    
    public void hddType() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Asks for user input for a hard drive type and then the capacity of that hard drive
        System.out.println("Please choose a hard drive (HDD or SSD): ");
        storageType = keyboard.nextLine();
        System.out.println("Please input a storage amount (in Gigabytes): ");
        storageCapacity = keyboard.nextInt();
    }
    
    public void chooseScreen() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Asks for a screen size
        System.out.println("Please choose a demension for your screen (in inches): ");
        screenSize = keyboard.nextInt();
    }
    
    public void setOSSpaceRequirements() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Asks what os the user wants to use
        System.out.println("What OS do you want installed on your computer? (Windows, Linux, MacOSX): ");
        os = keyboard.nextLine();
        
        //if the os equals any of these, then it sets the os storage requirements (in GB)
        if(os.equals("Windows")) {
            osCapacity = 19.0;
        }
        else if(os.equals("Linux")) {
            osCapacity = 9.6;
        }
        else if(os.equals("MacOSX")) {
            osCapacity = 26.0;
        }
        
        //Checks to see if the HDD or SSD is big enough to store the OS
        if(os.equals("Windows") &&  osCapacity < storageCapacity) {
            isOSInstalled = true;
        }
        else if(os.equals("Linux") &&  osCapacity < storageCapacity) {
            isOSInstalled = true;
        }
        else if(os.equals("MacOSX") &&  osCapacity < storageCapacity) {
            isOSInstalled = true;
        }
    }
    
    //Checks to see if there is valid ram, HDD or SSD, Case, and if there is an os installed so that you cant just type in anything and have it work
    public void runComputer() {
        if(ramType.equals("DDR1") || ramType.equals("DDR2") || ramType.equals("DDR3") || ramType.equals("DDR4")) {
            if(storageType.equals("SSD") || storageType.equals("HDD")) {
                if(caseType.equals("Full ATX") || caseType.equals("ATX") || caseType.equals("Mini ATX")) {
                    if(isOSInstalled = true) {
                        System.out.println(os + " is installed and your computer is fully functional!");
                    }
                }else{
                    System.out.println("ERROR: That is not a valid case. Try using (type exactly becuase case sensitive) 'ATX', 'Full ATX' or 'Mini ATX'");
                }
            }else{
                System.out.println("ERROR: Wrong type of storage installed. Please try again.");
            }
        }else{
            System.out.println("ERROR: Wrong memory installed. Please reinstall and try again.");
        }
    }
}