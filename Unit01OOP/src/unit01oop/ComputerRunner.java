package unit01javaoop;

/**
 * @author Jack Butler
 * Project Description: Make a program that uses methods, attributes, and constructors
 * Comments: Fully Functional
 */
public class ComputerRunner {

    public static void main(String[] args) {
        Computer c = new Computer();
        
        c.chooseCaseType();
        c.addMoreRam();
        c.cpuType();
        c.hddType();
        c.chooseScreen();
        c.setOSSpaceRequirements();
        c.runComputer();
    }
    
}
