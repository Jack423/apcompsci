/*
 * Author: Jack Butler
 * 
 * Project Description: Java Math
 *
 * Comments: None
 */

package unit01javaoop;
import java.util.Scanner;

public class JavaMath {
    public static void main(String[] args) {
        go: {
        Scanner keyboard = new Scanner(System.in);
        
        int type;
        /*
         * From line 21 - 78 is a menu that asks the user to input a number.
         * That number is stored as an int then used to determine what method
         * to run.
         */
        System.out.println("Choose a number to do that math problem");
        System.out.println("\\u001B[1: Calculate rectangles area and parimeter");
        System.out.println("2: Find the average of two numbers");
        System.out.println("3: Calculate circles circumfrence");
        System.out.println("4: Find the slope of two points");
        System.out.println("5: Convert Celcious to Ferenheit");
        System.out.println("6: Convert KMPH to MPH");
        System.out.println("7: Find the distance between to points");
        System.out.println("8: Find the roots of numbers (IN PROGRESS)");
        
        type = keyboard.nextInt();
        
        if(type == 1) {
            calcRectangle();
        }
        
        if(type == 2) {
            findAverage();
        }
        
        if(type == 3) {
            calcCircle();
        }
        
        if(type == 4) {
            findSlope();
        }
        
        if(type == 5) {
            convertCtoF();
        }
        
        if(type == 6) {
            findMPH();
        }
        
        if(type == 7) {
            findDistance();
        }
        
        if(type == 8) {
            findRoots();
        }
        
        if(type == 9000) {
            popeyesChickenIsFricknAwesome();
        }
        
        if(type > 8) {
            System.out.println("You can't choose a number higher than 8");
            break go;
        }
        
        if(type < 1) {
            System.out.println("You must choose a higher number");
            break go;
        }
    }
    }
    
    public static void calcRectangle() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //declare length and width of a rectangle
        int intLength;
        int intWidth;
        
        //Asks user to input a length and width. Then calculates this
        System.out.println("Enter a length: ");
        intLength = keyboard.nextInt();
        System.out.println("Enter a width: ");
        intWidth = keyboard.nextInt();
        System.out.println("");
        System.out.println("The area = " + intLength * intWidth);
        System.out.println("The perimeter = " + ((intLength * 2) + (intWidth * 2)));
    }
    
    public static void findAverage() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Decalre variables One and Two to find the average between them
        double numOne;
        double numTwo;
        
        //Asks for user input then calculates average
        System.out.println("Choose a number: ");
        numOne = keyboard.nextDouble();
        System.out.println("Choose another number: ");
        numTwo = keyboard.nextDouble();
        
        System.out.println("The average of the two numbers are: " + (numOne + numTwo));
    }
    
    public static void calcCircle() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Declare the variable radius
        double radius;
        
        //Asks for user input to store as an Int then calculates circumfrence
        System.out.println("Radius: ");
        radius = keyboard.nextDouble();
        
        System.out.println("The circumfrence of your circle is: " + 2 * Math.PI * radius);
    }
    
    public static void findSlope() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Declares variables for finding the slope
        int x1,x2,y1,y2;
        
        //Asks for user input for the 4 variables and calculates the slope
        System.out.println("Input X1: ");
        x1 = keyboard.nextInt();
        System.out.println("Input X2: ");
        x2 = keyboard.nextInt();
        System.out.println("Input Y1: ");
        y1 = keyboard.nextInt();
        System.out.println("Input Y2: ");
        y2 = keyboard.nextInt();
        
        //Prints the slope
        System.out.println("Slope = " + (y2 - y1) / (x2 - x1));
    }
    
    public static void convertCtoF() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Declares the variable c as the Celsius temperature
        double c;
        
        //Asks for user to determine the degres to convert to Ferenheit
        System.out.println("Celsius: ");
        c = keyboard.nextDouble();
        
        System.out.println(c + " Into Ferenheit is " + (c * 2) + 33.8);
    }
    
    public static void findMPH() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Declares kmph and mph to find mph with kmph
        int kmph;
        double mph;
        
        //Asks for user to input the kmph
        System.out.println("Choose a value in KMPH: ");
        kmph = keyboard.nextInt();
        
        //Converts to MPH
        mph = kmph * 0.621371;
        System.out.println(kmph + " Into MPH = " + mph);  
    }
    
    public static void findDistance() {
        //Use scanner method
        Scanner keyboard = new Scanner(System.in);
        
        //Declares the x and y values for the points
        double x1,x2,y1,y2,answer;
        
        //Asks the user to assign the values for the points
        System.out.println("X1: ");
        x1 = keyboard.nextDouble();
        System.out.print("X2: ");
        x2 = keyboard.nextDouble();
        System.out.println("Y1: ");
        y1 = keyboard.nextDouble();
        System.out.println("Y2: ");
        y2 = keyboard.nextDouble();
        
        //Calculates distance between the two points
        answer = Math.sqrt((Math.exp(x2 - x1)) - (Math.exp(y2 - y1)));
        System.out.println("The distance between (" + x1 + "," + x2 + ") and (" + y1 + "," + y2 + ") is " + answer); 
    }
    
     public static void findRoots(){
         //Use scanner method
         Scanner Keyboard = new Scanner(System.in);
         
         //Asks for the user to input values for a, b, and c for the equation
         System.out.println("Input a value for a:: ");
         double dblAValue = Keyboard.nextDouble();
         System.out.println("Input a value for b:: ");
         double dblBValue = Keyboard.nextDouble();
         System.out.println("Input a value for c:: ");
         double dblCValue = Keyboard.nextDouble();
         double dblAnswerOne = (-dblBValue+Math.sqrt((dblBValue*dblBValue)-(4*dblAValue*dblCValue))/(2/dblAValue));
         double dblAnswerTwo = (-dblBValue+Math.sqrt((dblBValue*dblBValue)+(4*dblAValue*dblCValue))/(2/dblAValue));
         
         //Calculates the answer
         System.out.println("The solutions of the equation y=" + dblAValue + "x^2+" + dblBValue + "x+" + dblCValue + " are " + dblAnswerOne + " and " + dblAnswerTwo + ". ");
         
     }
    
    public static void popeyesChickenIsFricknAwesome() {
        //All I have to say about this is that it is an inside joke. If you want to understand what the joke means, go to: https://www.youtube.com/watch?v=Gy-potQN01M
        System.out.println("Popeye's Chicken is Frickn' Awesome!");
    }

}
