package unit01javaoop;

/**
 * Description: To introduce manipulation using methods
 *              from Java's string class.
 * 
 * @author Jack Butler
 * 
 * Completion Comments: 
 */

public class MyString {
    //Identify the attributes/instance variables
    String s;
    
    //Constructor
    public MyString(String incommingStr) {
        s = incommingStr; //Set attribute to incommingStr
    }

    public MyString() {
        s = ""; //Set attribute to empty string
    }
    
    //Methods (Let the user change the string)
    public void SetString(String incStr) {
        s = incStr; //This is a setter method (aka modifyer for changing s)
    }
    
    // Demonstrate String concatenation
    // Take two strings and put them together as one
    public String glue2 (String a, String b) {
        return a + b;
    }
    
    // Demonstrate indexOf()
    // Demonstrate substring()
    // Take in a name (First Name) and return only the first name
    // Example: "Jack Butler" -----> "Jack"
    public String getFirst(String fullName) {
        int spaceLoc = fullName.indexOf(' ');
        return fullName.substring(0,spaceLoc);
    }

    // Demonstrate indexOf()
    // Demonstrate substring()
    // Take in a name (Last Name) and return only the last name
    // Example: "Jack Butler" -----> "Butler"
    public String getLast(String lastName) {
        int spaceLoc = lastName.indexOf(' ');
        return lastName.substring(spaceLoc + 1);
    }
    
    // Demonstrate length() (Retruns legths of strings)
    // Returns second half of a string
    public String secondHalf (String s1) {
        int midpoint = s1.length() / 2;
        return s1.substring(midpoint);
    }
}
