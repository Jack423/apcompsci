package unit01javaoop;

/**
 *
 * @author 12575
 */
public class MyStringRunner {
    public static void main(String[] args) {
        MyString tester = new MyString("Jack Butler");
        
        //tester.setString("Jack Butler");
        System.out.println(tester.getFirst("Jack Butler"));
        System.out.println(tester.getLast("Jack Butler"));
        System.out.println(tester.secondHalf("Jack Butler"));
    }
    
}
