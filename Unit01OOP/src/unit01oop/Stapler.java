package unit01javaoop;

/**
 * Lab Description: Practice building classes and using attributes and behaviors to affect objects in OOP.
 * 
 * Completion Notes: Fully functional.
 * 
 * @author Jack Butler
 */

    public class Stapler {
    
        //Instance Variables (a.k.a. State Variables, etc.):
        
        String color; //Color of the stapler.
        int stapleCount; //Number of staples in the stapler.
        String stapleSize; //Size of the staples in the stapler.
        
        public Stapler(){ //Constructor
            
            //Give instance variables (attributes) initial values
            
            color = "pink";
            stapleCount = 100;
            
        }
    
        //Methods (behaviors)
        
        public void staple(){
            
            stapleCount--;
            //stapleCount = stapleCount-1;
            //stapleCount -= 1;
            
        }
        
        public void removeStaples(){
            
            stapleCount = 0;
            
        }
        
        public void loadStaples(int howMany){
            
            stapleCount = stapleCount + howMany;
            //stapleCount += 
            
        }
        
        public void printMe(){
            
            System.out.println("This " + color + " stapler has " + stapleCount + " staples.");
            
        }
        
}