package unit01javaoop;

/**
 * Lab Description: Runner for Stapler.java
 * 
 * Completion Notes: Fully functional
 * 
 * @author Jack Butler
 */

    public class StaplerRunner {

        public static void main(String[] args) {
            
            //int myX = 8;
            Stapler deskStapler = new Stapler();
            
            deskStapler.printMe();
            deskStapler.staple();
            deskStapler.staple();
            deskStapler.staple();
            deskStapler.staple();           
            deskStapler.color = "bright pink";
            
            deskStapler.printMe();
            
            deskStapler.loadStaples(4);
            deskStapler.color = "pink";
            
            deskStapler.printMe();
            
        }
    
    }