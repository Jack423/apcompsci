package unit01javaoop;

import java.util.Scanner;

/**
 * Author: Jack Butler
 * Project Description: String Demo Lab
 * Comments: Use runner to use this file.
 */
public class StrStringDemo {

    public static void main(String[] args) {
        String MyString; 
        
        Scanner keyboard = new Scanner(System.in); 
        System.out.println("Please enter a sample string");
        MyString = keyboard.nextLine();
    }
    
    public char charAt(){
        int myInt;
        char answer;
        String MyString; 
        
        Scanner keyboard = new Scanner(System.in); 
        System.out.println("Please enter a sample string");
        MyString = keyboard.nextLine();
        System.out.println("Choose a number");
        myInt = keyboard.nextInt();
        answer = MyString.charAt(myInt);
        
        return answer;
    }
    
    public int compareTo(){
        Scanner keyboard = new Scanner(System.in);
        
        String s1, s2;
        int answer;
        
        System.out.println("Input s1: ");
        s1 = keyboard.nextLine();
        System.out.println("Input s2: ");
        s2 = keyboard.nextLine();
        
        answer = (s1.compareTo(s2));
        
        return answer;
    }
    
    public boolean endsWith() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1, s2;
        boolean answer;
        
        System.out.println("Input a String: ");
        s1 = keyboard.nextLine();
        System.out.println("Input another string: ");
        s2 = keyboard.nextLine();
        
        answer = (s1.endsWith(s2));
        
        return answer;
    }
    
    public boolean equals() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1, s2;
        boolean answer;
        
        System.out.println("Input a String: ");
        s1 = keyboard.nextLine();
        System.out.println("Input another string: ");
        s2 = keyboard.nextLine();
        
        answer = (s1.equals(s2));
        
        return answer;
    }
    
    public boolean equalsIgnoreCase() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1, s2;
        boolean answer;
        
        System.out.println("Input a String: ");
        s1 = keyboard.nextLine();
        System.out.println("Input another string: ");
        s2 = keyboard.nextLine();
        
        answer = (s1.equalsIgnoreCase(s2));
        
        return answer;
    }
    
    public int indexOf() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1;
        String c;
        int answer;
        
        System.out.println("Input a string: ");
        s1 = keyboard.nextLine();
        System.out.println("Input an char you want to know the index of: ");
        c = keyboard.nextLine();
        
        answer = (s1.indexOf(c));
        
        return answer;
    }
    
    public int indexOf2() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1, c;
        int startIndex;
        int answer;
        
        System.out.println("Input a letter: ");
        s1 = keyboard.nextLine();
        System.out.println("Inout a starting index: ");
        startIndex = keyboard.nextInt();
        System.out.println("Input an char you want to know the index of: ");
        c = keyboard.nextLine();
        
        answer = s1.indexOf(s1, startIndex);
        
        return answer;
    }
    
    public int indexOf3() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1, s2;
        int answer;
        
        System.out.println("Input a string: ");
        s1 = keyboard.nextLine();
        System.out.println("Input a substring you want to find in your original string: ");
        s2 = keyboard.nextLine();
        
        answer = (s1.indexOf(s2));
        
        return answer;
    }
    
    public int indexOf4() {
        Scanner keyboard = new Scanner(System.in);
        
        String s1, s2;
        int answer;
        
        System.out.println("Input a string: ");
        s1 = keyboard.nextLine();
        System.out.println("Input another string: ");
        s2 = keyboard.nextLine();
        
        answer = (s1.indexOf(s2));
        
        return answer;
    }
    
    public boolean isEmpty() {
        Scanner keyboard = new Scanner(System.in);
        
        String str;
        boolean answer;
        
        System.out.println("Input a string or leave blank: ");
        str = keyboard.nextLine();
        
        answer = str.isEmpty();
        
        return answer;
    }
    
    public int length() {
        Scanner keyboard = new Scanner(System.in);
        
        String str;
        int answer;
        
        System.out.println("Input a string: ");
        str = keyboard.nextLine();
        
        answer = str.length();
        return answer;
    }
    
    public String replace() {
        Scanner keyboard = new Scanner(System.in);
        
        String str, replacedText, answer;
        
        System.out.println("Input an incorrect string: ");
        str = keyboard.nextLine();
        System.out.println("Now input another string that will insert a string to make it correct: ");
        replacedText = keyboard.nextLine();
        
        answer = str.replace(str, replacedText);
        
        return answer;
    }
    
    public String replaceAll() {
        Scanner keyboard = new Scanner(System.in);
        
        String str, replacedText, answer;
        
        System.out.println("Input a string: ");
        str = keyboard.nextLine();
        System.out.println("Now input a string that you want to replace the old string with: ");
        replacedText = keyboard.nextLine();
        
        answer = str.replaceAll(str, replacedText);
        
        return answer;  
    }
    
    public boolean startsWith() {
        Scanner keyboard = new Scanner(System.in);
        
        String str1, str2;
        boolean answer;
        
        System.out.println("Choose a string: ");
        str1 = keyboard.nextLine();
        System.out.println("Choose a character to check if your string started with that char: ");
        str2 = keyboard.nextLine();
        
        answer = str1.startsWith(str2);
        
        return answer;
    }
    
    public String substring() {
        Scanner keyboard = new Scanner(System.in);
        
        String str, answer;
        int num;
        
        System.out.println("Input a string: ");
        str = keyboard.nextLine();
        System.out.println("Input a num that you want to find the substring at: ");
        num = keyboard.nextInt();
        
        answer = str.substring(num);
        return answer;
    }
    
    public String substring2() {
        Scanner keyboard = new Scanner(System.in);
        
        String str, answer;
        int num, numTwo;
        
        System.out.println("Input a string: ");
        str = keyboard.nextLine();
        System.out.println("Input a num that you want to start the substring at: ");
        num = keyboard.nextInt();
        System.out.println("Inout another num that you want the substring to end at");
        numTwo = keyboard.nextInt();
        
        answer = str.substring(num, numTwo);
        return answer;
    }
    
    public String toLowerCase() {
        Scanner keyboard = new Scanner(System.in);
        
        String letter, answer;
        
        System.out.println("Input a character: ");
        letter = keyboard.nextLine();
        
        answer = letter.toLowerCase();
        return answer;
    }
    
    public String toUpperCase() {
        Scanner keyboard = new Scanner(System.in);
        
        String letter, answer;
        
        System.out.println("Input a character: ");
        letter = keyboard.nextLine();
        
        answer = letter.toUpperCase();
        return answer;
    }
    
    public String trim() {
        Scanner keyboard = new Scanner(System.in);
        
        String str, answer;
        
        System.out.println("Input a string with spaces in it: ");
        str = keyboard.nextLine();
        
        answer = str.trim();
        return answer;
    }
}