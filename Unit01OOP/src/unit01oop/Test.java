package unit01javaoop;

/**
 *
 * @author Jack Butler
 */
public class Test {
    public double getDistance(int x1, int y1, int x2, int y2) {
        
        double d = Math.sqrt((Math.pow(y2 - y1, 2)) + (Math.pow(x2 - x1, 2)));
        
        return d;
    }
}
