package unit01javaoop;

/*
 * Lab Description:
 * 
 * Completion Notes:
 * 
 * @author Jack Butler
 */

public class ifLab1 {

    public static void main(String[] args) {
        
        // test the getDiscount method
        System.out.println("Discount price: " + getDiscount(42)); 

        // test the isEven method
        if (isEven(42) == true)
	System.out.println("It is EVEN");
        else
        System.out.println("It is ODD");

        
        // test the isEvenString method
        if (isEvenString("pneumonaultramicroscopicsilicovolcanoconeosis") == true)
	System.out.println("It is EVEN");
        else
        System.out.println("It is ODD");
        
        
        // test the getBiggest method that finds the largest of 3 ints
        System.out.println("The largest of the 3 is : " + getBiggest(42, 24, 420));
        
        
        //test the abcOrder method
        System.out.println(abcOrder("Childers", "Schauer") + " comes first alphabetically!");
        
    }
    
    // Determine discount price of a sale item.  Price: $0-$49.99 (10% off), $50-$99 (15% off), $100+ (20% off)
    // Input parameter: price         return: dicounted price
    public static double getDiscount ( double pr ) {
        
        //calculate discount for 0-49
        if (pr>=0.0 && pr<50.0)
            return .9*pr;
        
        //calculate discount for 50-99
        else if (pr>=50.0 && pr<100.0)
            return .85*pr;
        
        //calculate discount for 100+
        else if (pr>=100.0)
            return .80*pr;
        
        //negative value was entered
        else
            return pr;
        
    }
    

    // Determine if an integer is even.  Input parameter: any integer    return: boolean (true or false)
    public static boolean isEven(int isEvenInt) {
        
	if (isEvenInt % 2 == 0)
            
        return true;
        
        else 
            
        return false;
                
    }
    
    
    // Determine if a String is even (based on its length)  THIS METHOD MUST CALL THE isEven METHOD!!
    // Input parameter: any String      return: boolean (true or false)
    public static boolean isEvenString (String isEvenStr) {
        
        int isEvenStrLength = isEvenStr.length();
        
        if (isEvenStrLength % 2 == 0)
            
        return true;
        
        else
            
        return false;
	
    }
    

    // Determine the largest of 3 integers.     Input parameters: 3 integers       return: the largest integer
    public static int getBiggest (int int1, int int2, int int3) {
        
        int biggestInt; 
        if ( int1 > int2 && int1 > int3 )
            biggestInt = int1;
        else if ( int2 > int1 && int2 > int3 )
            biggestInt = int2;
        else 
            biggestInt = int3;
        
        return biggestInt;  
	
    }


    // Determine the ABC order of 2 strings.     
    // Input parameters: 2 Strings       return: the String that comes first in the alphabet
    public static String abcOrder (String misterChilders, String misterSchauer) {
        
	String firstString;
        int firstOfFirst = 67;
        int firstOfSecond = 83;
        
        if (firstOfFirst < firstOfSecond)
            
            firstString = misterChilders;
        
        else
            
            firstString = misterSchauer;
            
        return firstString;    
        
    }
    
}