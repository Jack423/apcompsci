package unit02loopsandlogic;

/**
 * Project Description: Use for loops
 * @author Jack Butler
 * Comments: None
 */
public class ForGreatness {
    
    public ForGreatness() {
        
    }
    
    public String reverseMe(String s) {
        String str = ""; //Declares empty string to have the final string inside.
        
        for(int i = s.length() - 1; i >= 0; i--) {
            str = str + s.charAt(i); //After interval was created, find character at interval
        }
        
        return str;
    }
    
    public String vowelCounter(String s) {
        String str = ""; //Declares empty string to have the final string inside
        int vowelAt = 0; //Instantiates vowelAt to start at 0
        for(int i = 0; i < s.length(); i++) {
            //Finds the vowel in a string by checking if it equals one of these
            if(s.charAt(i) == 'a' || s.charAt(i) == 'A' || s.charAt(i) == 'e' || s.charAt(i) == 'E' || s.charAt(i) == 'i' || s.charAt(i) == 'I' || s.charAt(i) == 'o' || s.charAt(i) == 'O' || s.charAt(i) == 'u' || s.charAt(i) == 'U'){
                if(vowelAt > 9) {
                    vowelAt = 0;
                }
                
                vowelAt ++;
                str = str + vowelAt;   //Makes a new string with vowelAt attached to it
                
                
            } else {
                str = str + s.charAt(i);
            }
        }
        
        return str;
    }
    
    public void triWord (String s) {
        String last; //Last outcome of the meathod
        
        for(int i = s.length(); i > 0; i--) {
            last = s.substring(0, i);//find substring from 0 - i and makes it equal to last
            System.out.println(last);
        }
    }
    
    public void timesTable(int a, int b) {
        System.out.println("Multiplication table for " + b);
        
        for(int i = 1;i <= a; i++) { 
            System.out.println(i + "   " + i * b); //Prints interval (i) and multiplys it by the initial value
            System.out.println();
        }
    }
    
    public void getStats(int x, int y) {
        int sum = 0; //Final sum of stats
        int evenCount = 0; //Even count of sum
        int oddCount = 0; //Odd count of sum
        
        for (int i = x; i <= y; i++) {
            sum += i; //Adds the interval i to sum every passthrough
            
            if(i % 2 == 0) {
                evenCount = evenCount + 1; //Checks for even numbers and if true then adds them to evenCount
                
            } else {
                oddCount = oddCount + 1; //Checks for odd numbers and if true then adds them to oddCount
            } 
        }
        
        //Print final results
        System.out.println("Sum: " + sum);
        System.out.println("Even Count: " + evenCount);
        System.out.println("Odd Count: " + oddCount);   
    } 
    
    boolean isPrime(int num) {
        for(int i = 2; i < num; i++) {
            if(num % i == 0) //When num is % i and == 0 then it not is prime
                return false;
        }
        return true;
}
    
}
