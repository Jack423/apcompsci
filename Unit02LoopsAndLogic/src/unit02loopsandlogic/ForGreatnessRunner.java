/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit02loopsandlogic;

/**
 *
 * @author 12575
 */
public class ForGreatnessRunner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ForGreatness whatFor = new ForGreatness();    // create a test object
        
        System.out.println(whatFor.vowelCounter("AEIOUaeiou87878alkjdaslwlejrlajflawjkflwj"));   		
        whatFor.triWord("hippo");
        whatFor.timesTable(5,6);
        System.out.println(whatFor.reverseMe("hello"));
        whatFor.getStats(5, 15);	
        System.out.println(whatFor.isPrime(17));
      }
}
    
