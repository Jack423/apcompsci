package unit02loopsandlogic;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Jack Butler
 * Project Description: Game Land Version 2.0
 * Comments: None
 */
public class GameLand2 {
    Scanner keyboard = new Scanner(System.in);
    
    //Declares all the variables to be used throught GL2 and GL2R
    public int randomNum1;
    public int randomNum2;
    public int die1;
    public int die2;
    public int turn;
    public int roll;
    public int places1 = 0;
    public int places2 = 0;
    public String name1;
    public String name2;
    public boolean loseTurnPlayer1;
    public boolean loseTurnPlayer2;
    public int round;
    public int gameOver = 0;
    
    public void start() {
        Scanner keyboard = new Scanner(System.in); //Instantiates Scanner meathod
        
        System.out.println("Please input player 1's name: ");
        name1 = keyboard.nextLine();
        System.out.println("Please input player 2's name: ");
        name2 = keyboard.nextLine();
    }
    
    public void rollDiePlayer1() {
        int min = 2; //Minimum number the two die can roll
        int max = 12; //Maximum number the two die can roll
        
        //Generates random number and sets it to the value of the die
        Random rand = new Random();
        randomNum1 = rand.nextInt((max - min) + 1) + min; 
        die1 = randomNum1;
        
        if(die1 == 7) { //Checks weather die is equal to 7
            if(places1 < 7) { //Checks if player has not moved 7 spaces
                places1 = 0;
            } else {
                places1 -= 7;
            }
        } else {
            if(die1 == 12 || die1 == 2) { //Checks weather die equals 12 or 2
                loseTurnPlayer1 = true; //Sets loseTurn to true and is refrenced in runner
            } else {
                places1 = die1 + places1; //Moves player if not 2 12 or 7
            }
        }
        if(places1 >= 100) { //Checks to see if one of the players is > 100
            gameOver = 1;
        } else if(places2 >= 100) {
            gameOver = 1;
        }
    }
    
    public void rollDiePlayer2() {
        int min = 2; //Minimum number the two die can roll
        int max = 12; //Maximum number the two die can roll
        
        Random rand = new Random();
        randomNum2 = rand.nextInt((max - min) + 1) + min; 
        die2 = randomNum2;
        
        if(die2 == 7) { //Checks weather die equals 7 
            if(places2 < 7){ //Checks weather player has moved more than 7 spaces
                places2 = 0;
            } else {
               places2 -= 7; 
            }
        } else {
            if(die2 == 12 || die2 == 2) { //Checks weather die equals 12 or 2
                loseTurnPlayer2 = true; //Makes looseTurnPlayer2 true and gets refrenced in GL2R
            } else {
                places2 = die2 + places2;
            }
        }
        
        if(places1 >= 100) { //Checks for game won
            gameOver = 1;
        } else if(places2 >= 100) {
            gameOver =1;
        }
    }
    
}
