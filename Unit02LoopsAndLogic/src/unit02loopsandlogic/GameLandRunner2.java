package unit02loopsandlogic;

/**
 * @author Jack Butler
 * Project Description: Game Land 2 Runner
 * Comments: None
 */
public class GameLandRunner2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GameLand2 gl2 = new GameLand2(); //Refrence to GL2
        
        //Declares variables necessary for file
        boolean p1Wins = false;
        boolean p2Wins = false;
        String winner = "";
        
        gl2.start(); //runs the name chooser and stuff like that
        
        if(gl2.gameOver == 0) {
            while(gl2.places1 <= 100 || gl2.places2 <= 100) { //Checks if places are greater than or equal to 100
                gl2.round += 1; //Makes round interval
                System.out.println("-----------------------------" + "ROUND:" + gl2.round + "---------------------------------"); //Tells apart each round
                gl2.rollDiePlayer1(); //Runs rollDicePlayer1 in GL2 
                gl2.rollDiePlayer2(); //Runs rollDicePlayer2 in GL2
                System.out.println("Player 1's roll was a: " + gl2.die1); //Prints p1's roll
                if(gl2.loseTurnPlayer1 == true){ //Checks if they rolled a 7
                    System.out.println("Oh no! Player 1 looses their turn! They are currently at: " + gl2.places1); //they rolled a 7 and now they either move back or go no where
                    gl2.loseTurnPlayer1 = false; //Sets loseTurnPlayer1 back to false
                } else {
                    System.out.println("Now Player 1 is on spot: " + gl2.places1); //Shows what spot their on
                }
        
                System.out.println("Player 2 roll a: " + gl2.die2);
                if(gl2.loseTurnPlayer2 == true){ //Checks weather loseTurnPlayer2 is false
                    System.out.println("Oh no! Player 2 looses their turn! They are now at: " + gl2.places2); //loseTurnPlayer2 is true and - 7 or none
                    gl2.loseTurnPlayer2 = false;
                }else{
                    System.out.println("Now Player 2 is on spot: " + gl2.places2);
                }
                
            }
        }
        //Checks for winner
        if(gl2.places1 > gl2.places2) {
            System.out.println("");
            System.out.println("Player 1 Wins!");
        } else {
            System.out.println("");
            System.out.println("Player 2 Wins!");
        }
    }
}
