/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit02loopsandlogic;

/**
 * @author Jack Butler
 * Project Description: Whiley Lab
 * Comments: None
 */
public class Whiley {
    public Whiley() {
        //Constructor
    }
    
    public void divisors(int num){
        int i = num;
        
        while(i > 0) {
            if(num % i == 0) { //Checks weather num is a devisor of i
                System.out.println(i); //Print devisor
            }
            i--; //Next number
        }
    }
    
    public boolean isPerfect(int num) {
        int i = 1;
        int sum = 0;
       
        //Checks devisors and then adds them to see if they are perfect
        while(i < num) {
            if(num % i == 0) { 
                sum = sum + i;
            } 
            i++;
            
        }
        
        return sum == num;
    }
    
    public void digitData(int num) {
        String str = ""; //Declares empty string for final result
        int count = 0; 
        int sum = 0;
        int i = num;
        
        System.out.println("Number: " + num);
        
        //Makes num into a string then finds the length of it
        str = str + num;
        count = str.length();
        System.out.println("Lengh: " + count);
        
        i = count;
        while(i > 0){
            sum += num % 10;
            num = num/10;
            i--;
        }
        
        System.out.println("Sum: " + sum);
        
        
    }
}

