/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unit02loopsandlogic;

/**
 * @author: Jack Butler
 * Project Description: Runner for Whiley
 * Comments: None
 */
public class WhileyRunner {

    public static void main(String[] args) {
		// create a test object
        Whiley wh = new Whiley();    

        // test divisors method
        wh.divisors(10);    

        // test perfect number method
        int numToTest = 45;
        
        if (wh.isPerfect(numToTest)){
            System.out.println(numToTest + " is perfect!");
        }else{
            System.out.println(numToTest + " is not perfect!");
        }
	
        
	

        // test digit data method
        wh.digitData(54);
    }
    
}
