package unit03scannerclasses;

/*
 * Lab Description: Practice using arrays in java.
 * Completion Notes:
 * @author Jack Butler
 */

public class ArrayFunHouse {
    //Default Constructor.
    public ArrayFunHouse(){  
            
    }
        
    //Add up all the numbers in an array together.
    public int getSum(int[] numArray, int start, int stop) {
        int sum = 0;
            
        for (int i = start; i <= stop; i++) {     
            sum += numArray[i];    
        }
            
        return sum;   
    }
        
    //Find the number of times one number occurs in an array.
    public int getCount(int[] numArray, int val) {
        int count = 0;
            
        for (int i = 0; i < numArray.length; i++) {
            if (numArray[i] == val) {
                count++;
            }
        }
            
        return count; 
    }
        
    //Remove all instances of one value from an array.
    public int[] removeVals(int[] numArray, int val) {
        int[] retArray = new int[numArray.length - getCount(numArray, val)];
        int retIndex = 0;
            
        for (int i = 0; i < numArray.length; i++) {
            if (numArray[i] == val) {
                retArray[retIndex] = numArray[i];
            }
        }
            
        return retArray;  
    }  
}