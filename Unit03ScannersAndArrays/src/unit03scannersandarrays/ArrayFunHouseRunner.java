package unit03scannerclasses;

/*
 * Author: Jack Butler
 * Project Description: Array Fun House Runner
 * Comments: None
 */

import java.util.Arrays;

public class ArrayFunHouseRunner
{
	public static void main( String args[] )
	{
            ArrayFunHouse fun = new ArrayFunHouse();
            
            int[] one = {7, 4, 10, 0, 1, 7, 6, 5, 3, 2, 9, 7};

            System.out.println(Arrays.toString(one));
            System.out.println("sum of spots 3-6  =  " + fun.getSum(one,3,6));
            System.out.println("sum of spots 2-9  =  " + fun.getSum(one,2,9));
            System.out.println("# of 4s  =  " + fun.getCount(one,4));
            System.out.println("# of 7s  =  " + fun.getCount(one,7));
            System.out.println("before removing all 7s " + Arrays.toString(one));
            one = fun.removeVals(one,9);
            System.out.println("after removing all 7s " + Arrays.toString(one));
            System.out.println("# of 7s  =  " + fun.getCount(one,9));

            int[] two = {4,2,3,4,6,7,8,9,0,10,0,1,7,6,5,3,2,9,9,8,7};

            System.out.println("\n\n"+Arrays.toString(two));
            System.out.println("sum of spots 3-16  =  " + fun.getSum(two,3,16));
            System.out.println("sum of spots 2-9  =  " + fun.getSum(two,2,9));
            System.out.println("# of 0s  =  " + fun.getCount(two,0));
		
            
            System.out.println("# of 4s  =  " + fun.getCount(two,4));
	}
}