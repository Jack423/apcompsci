package unit03scannerclasses;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/*
 * Author: Jack Butler
 * Lab Description: Simulate rolling a die and tally results.
 * Notes: Run this file to make the program work
 */

    public class Dice {

        public static void main(String[] args) throws IOException {
            
            //Set up for output to file.
            FileWriter toFile = new FileWriter("DiceResults.txt");
            PrintWriter writer = new PrintWriter(toFile);
            Scanner keyboard = new Scanner(System.in);
            int numRolls;
            int[] occurenceCounter = new int[6];
            String checkStr = "";        
            
            System.out.println("Choose the number of rolls:: ");
            
            numRolls = keyboard.nextInt();
            
            System.out.println();
            
            //Print each roll of the dice to a file.
            for(int i = 0; i < numRolls; i++) {
                
                writer.println((int) (Math.random()*6+1));
                
            }
            
            toFile.close();
            writer.close();
            
            //Set up for input to array from file.
            Scanner toArray = new Scanner(new File("DiceResults.txt"));
                           
            while(toArray.hasNext()){
                checkStr = toArray.nextLine();
                
                switch (checkStr) {
                    case "1":
                        occurenceCounter[0]++;
                        break;
                    case "2":
                        occurenceCounter[1]++;
                        break;
                    case "3":
                        occurenceCounter[2]++;
                        break;
                    case "4":
                        occurenceCounter[3]++;
                        break;
                    case "5":
                        occurenceCounter[4]++;
                        break;
                    case "6":
                        occurenceCounter[5]++;
                        break;
                }
            }
            
            System.out.println("Number of ones you rolled: " + occurenceCounter[0]);
            System.out.println("Number of twos you rolled: " + occurenceCounter[1]);
            System.out.println("Number of threes you rolled: " + occurenceCounter[2]);
            System.out.println("Number of fours you rolled: " + occurenceCounter[3]);
            System.out.println("Number of fives you rolled: " + occurenceCounter[4]);
            System.out.println("Number of sixes you rolled: " + occurenceCounter[5]);
                    
            toArray.close(); 
        }    
    }