package unit03scannerclasses;

/*
 * Lab Description: Practice using arrays in java. 
 * Completion Notes: 
 * @author Jack Butler
 */
public class Fibonacci {

    private int[] list;

    public Fibonacci() {
        list = new int[0]; //creates an array
    }

    public Fibonacci(int size) {
        setFibo(size); //Sets the size of the array
    }

    public void setFibo(int size) {
        list = new int[size];

        if (size > 0) {
            list[0] = 1;
        }
        if (size > 1) {
            list[1] += list[0];
        }
        if (size > 2) {
            for (int i = 2; i < size; i++) {
                list[i] += list[i - 1] + list[i - 2];
            }
        }
    }

    public int getFibo(int num) {
        int retInt = -1;

        if (num < list.length) {
            retInt = list[num];
        }

        return retInt;
    }
}
