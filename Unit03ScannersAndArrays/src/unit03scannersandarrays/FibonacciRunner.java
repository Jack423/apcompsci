package unit03scannerclasses;

/*
 * Lab Description: Practice using arrays in java.
 * Completion Notes: 
 * @author Tim Childers
 */
public class FibonacciRunner {

    public static void main(String[] args) {

        Fibonacci fib = new Fibonacci(50);

        //What is put into the code to get an output
        System.out.println(fib.getFibo(1));
        System.out.println(fib.getFibo(2));
        System.out.println(fib.getFibo(3));
        System.out.println(fib.getFibo(4));
        System.out.println(fib.getFibo(11));
        System.out.println(fib.getFibo(16));
        System.out.println(fib.getFibo(31));
        System.out.println(fib.getFibo(60));

        fib.setFibo(10);
        System.out.println(fib.getFibo(1));
        System.out.println(fib.getFibo(2));
        System.out.println(fib.getFibo(2));
        System.out.println(fib.getFibo(11));
    }
}
