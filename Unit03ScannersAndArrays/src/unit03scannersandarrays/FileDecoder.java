package unit03scannerclasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

/**
 * Author: Jack Butler Project Description: File Decoder Notes: Use this file to
 * run the program, no runner file needed
 */
public class FileDecoder {

    private static Writer lol;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        //Setup for scanner
        FileWriter solution = new FileWriter("solution.txt"); //Set up file writer
        Scanner code = new Scanner(new File("decodeMe.txt")); //Set up file reader
        PrintWriter lol = new PrintWriter(solution);
        
        while (code.hasNext()) {
            String in = code.next();
            String out = "";

            for (int i = 0; i < in.length(); i++) {
                char a = in.charAt(i);
                out += (char) (a - 1);
            }

            lol.println(out);
        }

        code.close();
        lol.close();
        solution.close();
    }

}
