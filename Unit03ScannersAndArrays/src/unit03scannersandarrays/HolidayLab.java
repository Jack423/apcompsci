package unit03scannerclasses;


//Imports for all the required stuff
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

/**
 * Author: Jack, Tim, Ben Project Description: Holiday Lab Notes: None
 */
public class HolidayLab {

    public HolidayLab() {
        //declares days as an array to be later used in the code
        String[] days = {"Partrage in a pear tree", "Two turtle doves", "Three French hens", "Four calling birds", "Five gold rings", "Six Geese a-laying", "Seven swans a-swimming", "Eight maids a-milking", "Nine ladies dancing", "Ten lords a leaping", "Eleven pipers piping", "Twelve drummers drumming"};
    }

    String[] days = {"Partrage in a pear tree", "Two turtle doves", "Three French hens", "Four calling birds", "Five gold rings", "Six Geese a-laying", "Seven swans a-swimming", "Eight maids a-milking", "Nine ladies dancing", "Ten lords a leaping", "Eleven pipers piping", "Twelve drummers drumming"};

    //Tim's Code (Combo of the constructor)
    public void whatDay(int day) {
        String dayFromArray = null;

        if (day == 1) {
            //sets to the "part" of the days array (Same for the ones below too)
            dayFromArray = days[0];
            
        }

        if (day == 2) {
            dayFromArray = days[1];
        }

        if (day == 3) {
            dayFromArray = days[2];
        }

        if (day == 4) {
            dayFromArray = days[3];
        }

        if (day == 5) {
            dayFromArray = days[4];
        }

        if (day == 6) {
            dayFromArray = days[5];
        }

        if (day == 7) {
            dayFromArray = days[6];
        }

        if (day == 8) {
            dayFromArray = days[7];
        }

        if (day == 9) {
            dayFromArray = days[8];
        }

        if (day == 10) {
            dayFromArray = days[9];
        }

        if (day == 11) {
            dayFromArray = days[10];
        }

        if (day == 12) {
            dayFromArray = days[11];
        }     
    }

    //Jack's Code
    public void prntToFile(String str) throws FileNotFoundException, IOException {
        //This doesn't do anything becuase the code that was supposed to go here made the porgam not work
    }

    //Bens Code
    public String retString() {
        //returns beautiful ascii art 
        return "      __,_,_,___)          _______\n"
                + "    (--| | |             (--/    ),_)        ,_) \n"
                + "       | | |  _ ,_,_        |     |_ ,_ ' , _|_,_,_, _  ,\n"
                + "     __| | | (/_| | (_|     |     | ||  |/_)_| | | |(_|/_)___,\n"
                + "    (      |___,   ,__|     \\____)  |__,           |__,\n"
                + "\n"
                + "                            |                         _...._\n"
                + "                         \\  _  /                    .::o:::::.\n"
                + "                          (\\o/)                    .:::'''':o:.\n"
                + "                      ---  / \\  ---                :o:_    _:::\n"
                + "                           >*<                     `:}_>()<_{:'\n"
                + "                          >0<@<                 @    `'//\\\\'`    @ \n"
                + "                         >>>@<<*              @ #     //  \\\\     # @\n"
                + "                        >@>*<0<<<           __#_#____/'____'\\____#_#__\n"
                + "                       >*>>@<<<@<<         [__________________________]\n"
                + "                      >@>>0<<<*<<@<         |=_- .-/\\ /\\ /\\ /\\--. =_-|\n"
                + "                     >*>>0<<@<<<@<<<        |-_= | \\ \\\\ \\\\ \\\\ \\ |-_=-|\n"
                + "                    >@>>*<<@<>*<<0<*<       |_=-=| / // // // / |_=-_|\n"
                + "      \\*/          >0>>*<<@<>0><<*<@<<      |=_- |`-'`-'`-'`-'  |=_=-|\n"
                + "  ___\\\\U//___     >*>>@><0<<*>>@><*<0<<     | =_-| o          o |_==_| \n"
                + "  |\\\\ | | \\\\|    >@>>0<*<<0>>@<<0<<<*<@<    |=_- | !     (    ! |=-_=|\n"
                + "  | \\\\| | _(UU)_ >((*))_>0><*<0><@<<<0<*<  _|-,-=| !    ).    ! |-_-=|_\n"
                + "  |\\ \\| || / //||.*.*.*.|>>@<<*<<@>><0<<@</=-((=_| ! __(:')__ ! |=_==_-\\\n"
                + "  |\\\\_|_|&&_// ||*.*.*.*|_\\\\db//__     (\\_/)-=))-|/^\\=^=^^=^=/^\\| _=-_-_\\\n"
                + "  \"\"\"\"|'.'.'.|~~|.*.*.*|     ____|_   =('.')=//   ,------------.      \n"
                + "  jgs |'.'.'.|   ^^^^^^|____|>>>>>>|  ( ~~~ )/   (((((((())))))))   \n"
                + "      ~~~~~~~~         '\"\"\"\"`------'  `w---w`     `------------'";
    }
}
