package unit03scannerclasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

/*
 * Author: Jack, Tim, Ben
 * Project Desicription: Holliday Lab runner that gets input from users
 * Notes: None
 */
/*
 _    _  ____  _      _____ _____      __     __  _               ____  _ 
 | |  | |/ __ \| |    |_   _|  __ \   /\\ \   / / | |        /\   |  _ \| |
 | |__| | |  | | |      | | | |  | | /  \\ \_/ /  | |       /  \  | |_) | |
 |  __  | |  | | |      | | | |  | |/ /\ \\   /   | |      / /\ \ |  _ <| |
 | |  | | |__| | |____ _| |_| |__| / ____ \| |    | |____ / ____ \| |_) |_|
 |_|  |_|\____/|______|_____|_____/_/    \_\_|    |______/_/    \_\____/(_)

 */
public class HolidayLabRunner {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        HolidayLab hl = new HolidayLab();
        Scanner keyboard = new Scanner(System.in);
        
        int day;
        
        hl.retString();
        System.out.println("What day is it (out of the 12 days of christmas)?: ");
        day = keyboard.nextInt();
        hl.whatDay(day);
        
        
    }
}
