package unit03scannerclasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Scanner;

/**
 * Author: Jack Butler
 * Project Description: Play with getting strings from files
 * Comments: None
 */
public class PlayWithFiles {
    private static Writer fw;

    public static void main(String[] args) throws IOException {
        //Setup for scanner
        Scanner sf = new Scanner(new File ("music.txt")); //Set up file reader  
        PrintWriter o = new PrintWriter(fw);
        FileWriter fw = new FileWriter("out.txt"); //Set up file writer
        
        //Sets up a counter for the while loop
        int counter = 0;
        
        while(sf.hasNext()){
            counter++;
            String s = sf.next(); //sets s to the next in sf
            o.println(s + "is a good band"); //
        }
        
        sf.close();
        
        for(int i = 0; i < counter; i++) {
            System.out.println(sf);
        }
        
        sf.close();
        fw.close();
        o.close();
    }
    
}
