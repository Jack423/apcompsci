package unit03scannerclasses;

import java.util.Scanner;

/**
 * Author: Jack Butler Project Description: Use Strings Notes: FHRITP!
 */
public class ScanWithStrings {

    public void digitData(String nums) {
        Scanner chopper = new Scanner(nums);

        //Setup for all the variables
        int count = 0;
        int sum = 0;
        int average = 0;
        int evens = 0;
        int odds = 0;

        //
        while (chopper.hasNext()) {
            int myNum = chopper.nextInt(); //Creates chopper for while loop
            sum += myNum; //Finds sum
            average = sum / myNum; //Finds average of myNum

            //Checks for even and odd numbers
            if (myNum % 2 == 0) {
                evens++;
            } else {
                odds++;
            }

            count++;
        }

        //Prints all the information needed
        System.out.println("Count: " + count + " | Sum: " + sum + " | Average: " + average + " | Evens: " + evens + " | Odds: " + odds);

    }

    public void lineBreaker(String str, int num) {
        Scanner chopper = new Scanner(str);

        while (chopper.hasNext()) {
            for (int i = num; i > 0; i--) {
                if (chopper.hasNext()) { //checks if chopper has any thing else to chop
                    System.out.print(chopper.next()); //prints final result
                }
            }
            System.out.println("");//for looks purposes only
        }
    }
}
