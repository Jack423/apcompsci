package unit03scannerclasses;

/**
 * Author: Jack Butler
 * Project Description: Runner for ScanWithStrings
 * Notes: None
 */
public class ScanWithStringsRunner {
    public static void main(String[] args) {
        // create a test object
        ScanWithStrings sws = new ScanWithStrings();    

        //test digit data method
        sws.digitData("5 4");
        sws.digitData("65 43 2 1"); 
        sws.digitData("4 4 4");

        // test lineBreaker method
        sws.lineBreaker("a b i g b a d w o l f h a s b i g e a r s", 2);    
        sws.lineBreaker("a c o m p u t e r s c i e n c e p r o g r a m", 7);
    }
}
