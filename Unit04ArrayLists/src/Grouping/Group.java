package Grouping;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Jack Butler Project Description: Make groups of celebrities Notes:
 * Use GroupMakerRunner to run this file
 */
public class Group {

    private ArrayList<String> names;
    private String fileName;

    public Group(String fn) {
        fileName = fn;

        names = new ArrayList<>();

        setList();
    }

    public void setList() {
        try { // load appropriate ArrayList from appropriate 
            Scanner fr = new Scanner(new File(fileName)); //Set up file reader
            while (fr.hasNext()) {
                names.add(fr.nextLine());
            }

        } catch (Exception e) {
            System.out.println("cannot find file Celebs.txt");
        }
    }

    /**
     * public void divide(int groups) { ArrayList<String> namesCopy = new
     * ArrayList<>(); for (String s : names) { namesCopy.add(s); } String[][] g
     * = new String[groups][1000];
     *
     * for (int row = 0; row < names.size(); row++) { for(int col = 0; col ==
     * groups; col++) { int rand = (int) (Math.random() * namesCopy.size());
     * g[row][col] = namesCopy.remove(rand); //g[count % groups][count / groups]
     * = namesCopy.remove(rand); System.out.print(g); } } System.out.print(g); }
     */
    public void divide(int groups) {

        //Make a copy so that we don't destroy the names.
        ArrayList<String> namesCopy = new ArrayList<String>();

        for (String s : names) {
            namesCopy.add(s);
        }

        int amntInGroup = namesCopy.size() / groups;

        for (int i = 1; i <= groups; i++) {

            System.out.println("\nGroup " + (i) + ":");
            int startSize = 1;
            if (names.size() % groups >= i) {
                startSize = 0;
            }

            for (int j = startSize; j <= amntInGroup; j++) {
                if (namesCopy.size() > 0) {
                    int location = (int) (Math.random() * namesCopy.size());
                    System.out.println(namesCopy.get(location));
                    namesCopy.remove(location);
                }
            }
        }
    }
}
