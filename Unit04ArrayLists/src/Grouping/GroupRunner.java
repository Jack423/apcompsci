package Grouping;

import static java.lang.System.*;

public class GroupRunner
{
	public static void main( String args[] )
	{
		Group rClass = new Group("APCSHeroes.txt");   // load our class 
		rClass.divide(5);  //split our class into 5 groups (print to console)
                System.out.println("");
		rClass.divide(10);  //split our class into 10 groups (print to console)
                System.out.println("");
		rClass.divide(2);  //split our class into 10 groups (print to console)
                System.out.println("");

		Group famous = new Group("Celebs.txt");
                
		famous.divide(11);   // 11 groups of celebs

		Group csGreats = new Group("CSHeroes.txt"); //CSHeros.txt doesn't work
		csGreats.divide(4);  // you get the idea
	}
}