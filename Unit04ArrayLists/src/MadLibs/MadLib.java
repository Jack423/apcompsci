package MadLibs;

/*
 * Author: Jack Butler
 * Project Description: Use ArrayLists to make a mad libs game
 * Notes: none
 */
import java.io.File;
import static java.lang.Math.random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public final class MadLib {

    private ArrayList<String> verbs;
    private ArrayList<String> nouns;
    private ArrayList<String> adjectives;

    public MadLib() {
        verbs = new ArrayList<String>();
        nouns = new ArrayList<String>();
        adjectives = new ArrayList<String>();

    }

    public MadLib(String template) {
        //load each ArrayList
        this();    	// calls the default constructor above
        loadNouns();
        loadVerbs();
        loadAdjectives();

        Scanner mlBuilder = new Scanner(template);  // loads template into scanner

        while (mlBuilder.hasNext()) {

            String currElement = mlBuilder.next();
            int location = template.indexOf(currElement);

            if (currElement.equals("#")) {
                String replaceNoun = getRandomNoun();

                //replace the char with the noun
                template = template.substring(0, location) + replaceNoun + template.substring(location + 1);

            } else if (currElement.equals("@")) {
                String replaceVerb = getRandomVerb();

                //replace the char with the verb
                template = template.substring(0, location) + replaceVerb + template.substring(location + 1);
            } else if (currElement.equals("&")) {
                String replaceAdj = getRandomAdjective();

                //replace the char with the adjective
                template = template.substring(0, location) + replaceAdj + template.substring(location + 1);
            } else {
                //Do nothing here --we don't need to modify these elements
            }
        }
        
       System.out.println(template);
    }
        // insert code here to...
    // loop through each element of the template
    // determine whether the element needs to be printed as is
    // or if a random item needs to be pulled from a file... and printed  	

    public void loadNouns() {

        try {// load appropriate ArrayList from appropriate 
            Scanner fr = new Scanner(new File("nouns.dat")); //Set up file reader
            while (fr.hasNext()) {
                nouns.add(fr.nextLine());
            }

        } catch (Exception e) {
            System.out.println("cannot find file nouns.dat");
        }

    }

    public void loadVerbs() {

        try {// load appropriate ArrayList from appropriate 
            Scanner fr = new Scanner(new File("verbs.dat")); //Set up file reader
            while (fr.hasNext()) {
                verbs.add(fr.nextLine());
            }

        } catch (Exception e) {
            System.out.println("cannot find file verbs.dat");
        }
    }

    public void loadAdjectives() {

        try {// load appropriate ArrayList from appropriate 
            Scanner fr = new Scanner(new File("adjectives.dat")); //Set up file reader
            while (fr.hasNext()) {
                adjectives.add(fr.nextLine());
            }

        } catch (Exception e) {
            System.out.println("cannot find file adjectives.dat");
        }
    }

    public String getRandomVerb() {
        int rand = (int) (Math.random() * verbs.size());
        return verbs.get(rand);
    }

    public String getRandomNoun() {
        int rand = (int) (Math.random() * nouns.size());
        return nouns.get(rand);
    }

    public String getRandomAdjective() {
        int rand = (int) (Math.random() * adjectives.size());
        return adjectives.get(rand);
    }

    public String toString() {
        return "\n\n\n";
    }
}
