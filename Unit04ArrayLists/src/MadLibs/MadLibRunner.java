package MadLibs;

import java.io.FileNotFoundException;
import static java.lang.System.*;
import MadLibs.MadLib;

/*
 * Author: Jack Butler
 * Project Description: MadLibs Runner
 * Notes: None
 */
public class MadLibRunner {

    public static void main(String args[]) throws FileNotFoundException {
        //make 3 MadLibs... 1st is given 
        MadLib ml1 = new MadLib("The # @ after the & & # while the # @ the #");
        
        out.println("\n");

        // insert 2 more here
    }
}
