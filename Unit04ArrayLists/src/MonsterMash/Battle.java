package MonsterMash;

/**
 * @author Jack Butler
 * Project Description: Have monsters battle and stuff
 * Notes: Use MonsterMash2 to run main game
 */
public class Battle extends Thread {

    //Creates variables for command purposes
    int powerOfAttack = 0;
    boolean didOneRunAway = false;
    boolean didTwoRunAway = false;
    boolean playerOneTurn = false;
    boolean playerTwoTurn = false;

    public void executeTurn(String attack, Monster playerOneMonster, Monster playerTwoMonster, boolean pt1, boolean pt2) {
        playerOneTurn = pt1;
        playerTwoTurn = pt2;
        //Checks to see whos turn it is
        if (playerOneTurn == true) {
            //checks to see what move they typed in
            if (attack.equals("scratch")) {
                //Sets the power of the attack to the monstrs health / 3
                powerOfAttack = playerOneMonster.getPower() / 3;
                //Applies the attack damage to opponents monster
                playerTwoMonster.setHealth(playerTwoMonster.getHealth() - powerOfAttack);
            } else if (attack.equals("punch")) {
                //Sets the power of the attack to the monstrs health / 4
                powerOfAttack = playerOneMonster.getPower() / 4;
                //Applies the attack damage to opponents monster
                playerTwoMonster.setHealth(playerTwoMonster.getHealth() - powerOfAttack);
            } else if(attack.equals("run away")) {
                didOneRunAway = true;
            } else if(attack.equals(05200423)) {
                playerTwoMonster.setHealth(0);
            }           
        } else if(playerTwoTurn == true) {
            if (attack.equals("scratch")) {
                //Sets the power of the attack to the monstrs health / 3
                powerOfAttack = playerTwoMonster.getPower() / 3;
                //Applies the attack damage to opponents monster
                playerOneMonster.setHealth(playerOneMonster.getHealth() - powerOfAttack);
            } else if (attack.equals("punch")) {
                //Sets the power of the attack to the monstrs health / 3
                powerOfAttack = playerTwoMonster.getPower() / 4;
                //Applies the attack damage to opponents monster
                playerOneMonster.setHealth(playerOneMonster.getHealth() - powerOfAttack);
            } else if(attack.equals("run away")) {
                //If the users decided to run away, then they lose
                didTwoRunAway = true;
            } else if(attack.equals(05200423)) {
                playerOneMonster.setHealth(0);
            }
        }
        
        //Prints the current state of the monsters
        System.out.println(playerOneMonster);
        System.out.println(playerTwoMonster);
    }
}
