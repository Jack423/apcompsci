package MonsterMash;

/**
 * @author Jack Butler 
 * Project Description: Monster Mash 
 * Notes: Use MonsterMashRunner to run this class
 */
public class Monster {

    //Instantiates variables for name, health, and power
    private String name;
    private int health;
    private int power;

    //Constructer methods to take in values
    public Monster() {
        name = "";
        health = 0;
        power = 0;
    }

    public Monster(String n) {
        name = n;
        health = 0;
        power = 0;
    }

    public Monster(String n, int p, int h) {
        name = n;
        health = h;
        power = p;
    }

    //All the getters that return a value weather it is a string or int
    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public int getHealth() {
        return health;
    }
    
    //All the setters that set names and values for power and health
    public void setName(String n) {
        name = n;
    }

    public void setHealth(int h) {
        this.health = h;
    }

    public void setPower(int p) {
        power = p;
    }
    
    public void doubleBackTrippleExplosiveAwesomeKick(int health, Monster obj) {
        
    }

    //Just returns the values of name, power, and health
    public String toString() {
        return("\n=============NAME==============\n" + name + 
               "\n=============POWER=============\n" + power + 
               "\n============HEALTH=============\n" + health + "\n");
    }
}
