package MonsterMash;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * @author: Jack Butler
 * Project Description: Have monsters battle and stuff
 * Notes: Use this file to run the main program
 */

public class MonsterMash2 {

    //Create a scanner so the user can input commands
    private static Scanner keyboard = new Scanner(System.in);

    //Create an array of monsters
    private static Monster[] monsters;

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {

        //Display a nice welcoming message
        System.out.println("Welcome to WarWorld!");

        createMonsters();

        //run the interactive portion of the code
        //Make a boolean to see if we need to keep running and a String to input a command
        boolean running = true;
        boolean playerOneWinenr = false; //Make boolean to see if anyone has one
        boolean playerTwoWinner = false;
        boolean playerOneTurn = true; //Sets up who goes first
        boolean playerTwoTurn = false;
        String command = null; //Players command
        String attack = null; //Players attack command
        Battle battle = new Battle();

        System.out.println("\nWhat would you like to do? (Battle or Quit): ");
        command = keyboard.next();
        //Enter main game loop
        while (running = true) {
            //Checks the command that the user typed in
            if(command.equals("battle") || command.equals("Battle")) {                                  
                
                //Prints stats so that user can decide what attack they want to use
                System.out.println("Here are the stats for you and your oponenets monster: ");
                System.out.println(monsters[0]);
                System.out.println(monsters[1]);
                System.out.println("Please choose a move that you would like to make on your oponent (Scratch, punch, run away, or ??)");
                attack = keyboard.next();
                //Executes battle move after the user inputed a move
                battle.executeTurn(attack, monsters[0], monsters[1], playerOneTurn, playerTwoTurn);
                
                //checks to see the health of both of the monsters to see if anyone died after the move
                if(monsters[0].getHealth() <= 0) {
                    System.out.println("Player 2 has one the game");
                    running = false;
                } else if(monsters[1].getHealth() <= 0) {
                    System.out.println("Player 1 has one the game");
                    running = false;
                } 
                
                //Switches player
                if(battle.playerOneTurn == true) {
                    playerOneTurn = false;
                    playerTwoTurn = true;
                } else if(battle.playerTwoTurn == true) {
                    playerOneTurn = true;
                    playerTwoTurn = false;
                }
            }
        }

    }

    private static ArrayList<String> getNames(int numNames) throws FileNotFoundException {

        //Make a scanner to load names
        Scanner nameList = new Scanner(new File("monsters.txt"));

        //Make an arrayList to hold all of the names
        ArrayList<String> names = new ArrayList<String>();

        //Make an arrayList to store the random names from the master list
        ArrayList<String> randNameList = new ArrayList<String>();

        while (nameList.hasNext()) {
            names.add(nameList.nextLine());
        }

        //Store the names in a new array and remove the names from the original array
        for (int i = 0; i < numNames; i++) {
            int rand = (int) (Math.random() * names.size());
            randNameList.add(names.get(rand));
            names.remove(rand);
        }

        //return the arrayList
        return randNameList;

    }

    //Setup the Monster array
    private static void createMonsters() throws FileNotFoundException {

        //Get a random name list from a name file
        ArrayList<String> names = getNames(2);

        //Make an array of Monsters
        monsters = new Monster[2];
        for (int i = 0; i < 2; i++) {

            int randPower = (int) (Math.random() * 100 + 1);
            monsters[i] = new Monster(names.get(i), randPower, 100);

        }
    }
}
