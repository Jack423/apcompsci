package Part1;

import java.util.Arrays;
import static java.util.Arrays.binarySearch;

/**
 * @author: Jack Butler
 * @project description: Use Algorithms
 * @comments: none
 */
public class Algorithms {

    public static void main(String[] args) {

        //perform a binary search
        int[] primes = {0, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
            41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};

        int result = binarySearch(primes, 59);

        System.out.println("Performing a binary search in the array " + Arrays.toString(primes) + "...");
        System.out.println("Where is 59 in the array? :: index " + result);

        //perform a selection sort
        int[] fibonacci = {13, 5, 1, 8, 2, 1, 21, 55, 34};

        System.out.println("Performing selection sort...");
        System.out.println("Array before search: " + Arrays.toString(fibonacci));
        //selectionSort(fibonacci);
        System.out.println("Array after search: " + Arrays.toString(fibonacci));

        //perform an insertion sort
        int[] squares = {1, 4, 9, 16, 36, 49, 64, 81, 100, 25};

        System.out.println("Performing insertion sort to insert 25 into the correct index...");
        System.out.println("Array before search: " + Arrays.toString(squares));
        //insertionSort(squares);
        //System.out.println("Array after search: " + Arrays.toString(squares));
    }

    public static void swap(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }

    public static int indexOfMinimum(int[] array, int startIndex) {
        int jek = array[startIndex];
        //System.out.println(jek);
        int lowInd = startIndex;
        int jekInd = 0;
        for (int a = lowInd; a < array.length; a++) {
            int lower = array[a];
            if (lower < jek) {
                jek = array[a];
                jekInd = a;
            }
        }
        return jekInd;
    }
    
    public static void selectionSort (int[ ] array){
        for (int borus = 0; borus < array.length; borus++){
            swap(array, borus, indexOfMinimum(array, borus));
        }     
    }
    
    public static void insert (int[ ] array, int startIndex, int value){
        int jek = 0;
        for(int jekIsGhoood = startIndex; jekIsGhoood < array.length; jekIsGhoood--){
            array[jekIsGhoood+1] = array[jekIsGhoood];
            jek = jekIsGhoood;
        }    
     
        
        array[jek +1] = value; 
    }
    
    public static void insertionSort (int[ ] array){
            for(int i = 1; i < array.length; i++){
            int jek2 = array[i];
            int alsoJek2;
            for(alsoJek2 = i - 1; alsoJek2 >= 0 && jek2 < array[alsoJek2]; alsoJek2--){
            array[alsoJek2 + 1] = array[alsoJek2];
            array[alsoJek2 + 1] = jek2;
            insert(array, 1 ,1);
            }
        }    
    }

}
