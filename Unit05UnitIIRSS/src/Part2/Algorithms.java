package Part2;

import java.util.Scanner;

/**
 * @author Jack butler
 */
public class Algorithms {

    public static void main(String[] args) {
        //Calls the main methods of the program and gives them parameters to use
        factorial(5);
        recursiveFact(5);
        palindrome("pot");
    }

    public static int factorial(int n) {
        int answer = 1;

        for (int i = 2; i <= n; i++) { //Goes in incriments and multiplies the answer by the incrament
            answer *= i;
        }

        System.out.println(answer);
        return answer;
    }

    public static int recursiveFact(int n) { //Uses recursion instead of a for loop
        if (n == 0) {
            return 1;
        } else {
            n *= recursiveFact(n - 1);
        }
        return n;
    }

    public static boolean palindrome(String word) {
        String changedWord = "";
        int x = word.length();
        if (x <= 1) {
            return true;
        } else if (firstCharacter(word) != lastCharacter(word)) {
            return false;
        } else {
            return palindrome(word.substring(1, x - 1));
        }
    }

    public static char firstCharacter(String word) {
        return word.charAt(0); //Gets the first character in a string
    }

    public static char lastCharacter(String word) {
        return word.charAt(word.length() - 1); //Gets the last character in a string
    }

    public static String middleCharacters(String pali) {
        String s = ""; //Creates a blank string to put the two middle characters into
        for (int i = 1; i < (pali.length() - 1); i++) {
            s = s + pali.charAt(i); //Loops through to find the middle two or one character
        }
        return s; //Returns the two or one chars
    }

    public static double recursivePow(double x, double n) {
        return n == 0 ? 1 : recursivePow(x, n - 1) * x; //Does recursive power
    }

    public static void mergeSort(int array[], int p, int q) {
        if ((q - p) <= 1) {
            if (array[q] < array[p]) { //Pulls the two values out of the two sub arrays and checks to see if q is less than p
                int temp = array[q];
                array[q] = array[p];
                array[p] = temp;
            }
        } else {
            mergeSort(array, p, p + (q - p) / 2);
            mergeSort(array, p + (q - p) / 2, q);
            merge(array, p, p + (q - p) / 2, q); //Calls the merge method to merge the two sub arrays
        }
    }

    public static void merge(int[] array, int start, int mid, int end) {
        /*
         * Method:
         * start-mid is the first array
         * mid + 1 to end is second array
         * )
         * Take the two arrays, and look at the first value.  
         * Take the smaller, and remove it, and add it to your final array
         * Repeat until done
         */
        //System.out.println(Arrays.toString(array));
        int p = start; //p is counter for first array
        int q = mid; //q is counter for second
        int[] newArr = new int[end - start + 1];
        for (int i = 0; i < newArr.length; i++) {
            if (p == mid) { //if the first arry has been filled
                newArr[i] = array[q++]; //just dump the next data from the other array and continue
                continue;
            } else if (q == end) {
                newArr[i] = array[p++];
                continue;
            }
            /*
             * sets the value to the lower of the two arrays and increments the counters
             */
            newArr[i] = (array[p] < array[q]) ? array[p++] : array[q++];
        }

        for (int i = 0; i < newArr.length; i++) {
            array[start + i] = newArr[i];
        }
    }
}
